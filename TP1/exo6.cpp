#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int nombreNoeud;
    //Noeud* dernier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{

    liste->premier = NULL;
    liste->nombreNoeud = 0;
    //liste->dernier = NULL;

}

bool est_vide(const Liste* liste)
{
    if(liste->nombreNoeud == 0){
        return true;
    } else {

        return false;
}

}

void ajoute(Liste* liste, int valeur)
{
    Noeud* n = (Noeud*)malloc(sizeof (*n));

        n->donnee = valeur;
        n->suivant = NULL;


        if(!liste->premier){

            liste->premier = n;
        }


        else {
            Noeud* m = liste->premier;


            while(m->suivant != NULL){
                m = m->suivant;
            }

            m->suivant = n;

        }




}

void affiche(const Liste* liste)
{
    int count = 1;
        Noeud* temp = liste->premier;

        while(temp != NULL)

        {
            cout << temp->donnee << endl;

            temp = temp->suivant;
            count ++;
        }


}

int recupere(const Liste* liste, int n)
{

    if( (n>=0) && (n<=liste->nombreNoeud) ){

            Noeud* temp = liste->premier;

            for(int i=0; i<n; i++){
                temp = temp->suivant;
            }

            return temp->donnee;

        } else {

        return -1;

}

}

int cherche(const Liste* liste, int valeur)
{

    Noeud* temp = liste->premier;

        for(int i=0 ; i < liste->nombreNoeud ; i++){

            if(temp->donnee == valeur){
                return i;
            }
            else{
                temp=temp->suivant;

            }
        }
        return -1;
}

void stocke(Liste* liste, int n, int valeur)
{

    if( (n>=0) && (n<=liste->nombreNoeud))
        {
            Noeud* temp = liste->premier;
            for(int i = 0 ; i<n-1 ; i++)
            {
                temp = temp->suivant;
            }
            temp->donnee = valeur;
        }
        else
        {
            cout << "Veuillez entrer un rang présent dans la liste"<< endl;
        }


}

void ajoute(DynaTableau* tableau, int valeur)
{

    if(tableau->taille == tableau->capacite){
            int * temp = new int[tableau->capacite + 1];

            for(int i=0; i<tableau->capacite; i++){
                temp[i] = tableau->donnees[i];
            }
            temp[tableau->capacite] = valeur;

            free(tableau->donnees);

            tableau->donnees = temp;
            tableau->capacite = tableau->capacite + 1;
        }else{
            tableau->donnees[tableau->taille] = valeur;
        }
        tableau->taille = tableau->taille + 1;

}


void initialise(DynaTableau* tableau, int capacite)
{

    tableau->taille = 0;
    tableau->capacite = capacite;
    tableau->donnees = new int[capacite];


}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->taille == 0){

            return true;

        } else {

            return false;
        }
}

void affiche(const DynaTableau* tableau)
{
    cout << "Tableau : " << endl;

    for(int i=0; i<tableau->taille; i++){

            cout << "Valeur " << i << " : " << tableau->donnees[i] << endl;
     }

        cout << endl;

}

int recupere(const DynaTableau* tableau, int n)
{

    if(n>=0 && n <tableau->taille){

            return tableau->donnees[n];

        }else{
            cout << "Le rang n'existe pas" << endl;
            return -1;
        }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{

    int count = 0;
    int index = -1;
    bool cherche = false;

        while(!cherche && count < tableau->taille)
        {
            if(tableau->donnees[count] == valeur)
            {
                index = count;
                cherche = true;
            }
            count ++;
        }
        return index;

}

void stocke(DynaTableau* tableau, int n, int valeur)
{

    if((n>=0) && (n < tableau->taille)){
             tableau->donnees[n] = valeur;

        }else{

            cout << "Le rang n'existe pas" << endl;
        }

}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{

    ajoute(liste, valeur);

}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{

    Noeud* temp = liste->premier;
        int valeur = temp->donnee;
        liste->premier = liste->premier->suivant;
        liste->nombreNoeud = liste->nombreNoeud-1;
        free(temp);
        return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{

    Noeud* nouveau = new Noeud;

        nouveau->donnee = valeur;
        nouveau->suivant = liste->premier;

        liste->premier = nouveau;
        liste->nombreNoeud = liste->nombreNoeud +1;


}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{

    Noeud* temp = liste->premier;
        int valeur = temp->donnee;

        liste->premier = liste->premier->suivant;
        liste->nombreNoeud = liste->nombreNoeud -1;

        free(temp);
        return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
